//register and login buttons
var registerb = document.getElementById("registerb");
var loginb = document.getElementById("loginb");


//register division components
var fname = document.getElementById("fname");
var email = document.getElementById("email");
var mobno = document.getElementById("mobno");
var address = document.getElementById("address");
var pass1 = document.getElementById("pass1");
var pass2 = document.getElementById("pass2");


//login divison components
var lemail = document.getElementById("lemail");
var lpass = document.getElementById("lpass");


//submit function for register button
registerb.onsubmit = function (e) {
    e.preventDefault();

    if (pass1.value !== pass2.value) {
        alert("passwords dont match");
        return;
    }

    var hr = new XMLHttpRequest();
    hr.onreadystatechange = function () {
        if (hr.readyState !== 4)
        return;

        if (hr.status === 200) {
             alert(hr.responseText);
        } else if (hr.status === 404) {
            alert("page not found");
        } else if (hr.status === 500) {
            alert("Internal server error");
        } else {
            alert("some error occured");
        }
    }
    hr.open('POST', 'http://localhost/hackathon/php/register.php');
    hr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    hr.send('fname=' + fname.value  + "&email=" + email.value + "&mobno=" + mobno.value
    + "&address=" + address.value + "&pass=" + pass1.value);
}


//submit function for login button
loginb.onsubmit = function (e) {
    e.preventDefault();

    var hr = new XMLHttpRequest();
    hr.onreadystatechange = function () {
        if (hr.readyState !== 4)
        return;

        if (hr.status === 200) {
           
           if(hr.responseText === "1")
           {
              window.location.href="home.html";
           }
           else
           {
              alert("credentials incorrect");
           }
        } else if (hr.status === 404) {
            alert("page not found");
        } else if (hr.status === 500) {
            alert("Internal server error");
        } else {
            alert("some error occured");
        }
    }
    
    hr.open('POST', 'http://localhost/hackathon/php/login.php');
    hr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    hr.send("lemail=" + lemail.value + "&pass=" + lpass.value);
}