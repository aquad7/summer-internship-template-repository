<?php
    if(!isset($_COOKIE['email'])) {
        echo "<script>window.location.href='http://localhost/hackathon/'</script>";
    } else {
		include('DBconnect.php');
		$email = $_COOKIE['email'];
		$result = mysqli_query($conn,"select customerid from customer where email='$email'");
		$row = mysqli_fetch_array($result);
		if($row[0] == null)
		{
			echo "<script>window.location.href='localhost/hackathon/'</script>";
		}
		$custid = $row[0];
	}
?>
<?php
    $modelno = $_GET['modelno'];
	$serviceno= $_GET['serviceid'];
    
    $result=mysqli_query($conn,"select * from services where serviceno='$serviceno'");
    $row=mysqli_fetch_array($result);
	if($row[0] == null)
	{
		echo "<script>window.location.href = 'http://localhost/hackathon/home.html'</script>";
	}
?>
<html>
    <head>
        <title>Service</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../css/home.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" href="animate.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
    </head>
    <body>
        <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-header" href="#myPage"><img src="image/icon (2).png" class="dell-icon-3" ></a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://localhost/hackathon/home.html"><span class="glyphicon glyphicon-home"></span> HOME</a></li>
            </ul>
            </div>
        </div>
        </div>
        <div class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <center class="col-md-6 product-details">
                <img class="image" src="http://localhost/hackathon/service-image/<?php
                    echo $row['serviceno'];
                ?>.png" height="200" width="300" class="img-thumbnail" /><br/><br/>
                <b style="color:#4040ec;">Service Name</b> : <?php echo $row['servicename']; ?><br/><br/>
                <b style="color:#4040ec;">Service Type</b> : <?php echo $row['servicetype']; ?><br><br/>
				<b style="color:#4040ec;">Service Cost</b> : <?php echo $row['servicecost']; ?><br><br/>
				<b style="color:#4040ec;">Description</b>  : <?php echo $row['description']; ?><br><br/>
				<button class="btn" id="buybtn">BUY</button>
            </center>
            <div class="col-md-3"></div>
        </div>    
        </div>
		<script>
             var x = "<?php echo $modelno ?>";
		     var buybtn=document.getElementById("buybtn");
			 buybtn.onclick = function(){
                 if (x === "") {
                    x = prompt("Please enter the model No.", "ex : 10001");
                 } else {
                    var hr = new XMLHttpRequest();
                    hr.onreadystatechange = function () {
                        if (hr.readyState !== 4) {
                            return;
                        }
                        
                        if (hr.status === 200) {
                            if (hr.responseText != 1) {
                                alert(hr.responseText);
                            } else {
                                alert("purchased service successfully!");
                                window.location.href = "http://localhost/hackathon/home.html";
                            }
                        } else if (hr.status === 404) {
                            alert('page not found');
                        } else if (hr.status === 500) {
                            alert('internal server error');
                        } else {
                            alert('some error occurred');
                        }
                    }
                    hr.open('POST', 'http://localhost/hackathon/php/buy.php');
                    hr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    hr.send("modelno=" + x + "&serviceno=" + <?php echo $serviceno ?> + "&customerid=" + <?php echo $custid ?>);
                 }
                 
			 }
		</script>
    </body>
</html>