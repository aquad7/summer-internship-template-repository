-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 04, 2019 at 07:57 PM
-- Server version: 10.3.16-MariaDB
-- PHP Version: 7.1.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sre`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `customerid` int(9) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `mobno` varchar(10) NOT NULL,
  `address` varchar(1000) NOT NULL,
  `age` INT(9) NOT NULL,
  `pass` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`customerid`, `fname`, `email`, `mobno`, `address`, `pass`,`age`) VALUES
(1, 'Biswajit Sam', 'biswa@gmail.com', '9988967567', 'some address', 'qwerty','19'),
(2, 'Jai Shri Ram', 'jai@gmail.com', '9090987654', 'some addresss3', 'qwerty','20'),
(3, 'Praveen Sahadev', 'praveen@gmail.com', '9988889978', 'some address2', 'qwerty','21'),
(4, 'Tirthankar Nayak', 'tirtha@gmail.com', '9078813658', 'some address', 'qwerty','20'),
(5, 'Venkat', 'venky@gmail.com', '9908830052', 'some sample address', 'qwerty','19');

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(9) NOT NULL,
  `customerid` int(9) NOT NULL,
  `modelno` varchar(50) NOT NULL,
  `serviceno` int(10) NOT NULL,
  `rating` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `customerid`, `modelno`, `serviceno`, `rating`) VALUES
(1, 1, '10001', 7, 8),
(2, 1, '10001', 7, 10),
(3, 2, '10001', 1, 6),
(4, 1, '10001', 8, 4),
(5, 2, '10001', 9, 9),
(6, 4, '10001', 1, 6),
(7, 4, '10001', 3, 9),
(8, 1, '10001', 1, 10),
(9, 1, '10001', 2, 7),
(10, 2, '10001', 2, 9),
(11, 2, '10001', 2, 10),
(12, 2, '10001', 3, 7),
(13, 2, '10001', 3, 3),
(14, 3, '10001', 4, 10),
(15, 2, '10001', 4, 9),
(16, 2, '10001', 5, 1),
(17, 2, '10001', 5, 5),
(18, 2, '10001', 6, 10),
(19, 2, '10001', 6, 10),
(20, 2, '10001', 7, 7),
(21, 2, '10001', 7, 8),
(22, 2, '10001', 8, 10),
(23, 2, '10001', 8, 10),
(24, 2, '10001', 9, 1),
(25, 2, '10001', 9, 5),
(26, 2, '10001', 4, 10),
(27, 1, '10001', 3, 9),
(28, 1, '10001', 3, 9),
(29, 1, '10001', 4, 10),
(30, 1, '10001', 7, 10),
(31, 1, '10001', 7, 10),
(32, 1, '10001', 9, 2),
(33, 1, '10001', 6, 7),
(34, 4, '10001', 4, 10),
(35, 4, '10001', 4, 10),
(39, 1, '10002', 1, 7),
(40, 1, '10002', 2, 5),
(41, 1, '10002', 3, 9),
(42, 2, '10002', 4, 8),
(43, 2, '10002', 5, 6),
(45, 2, '10002', 6, 9),
(46, 3, '10002', 7, 3),
(47, 3, '10002', 8, 8),
(48, 3, '10002', 9, 2),
(49, 4, '10002', 3, 7),
(50, 4, '10002', 6, 6),
(51, 4, '10002', 9, 4),
(52, 5, '10001', 2, 8),
(53, 5, '10001', 4, 9),
(54, 5, '10001', 6, 6),
(55, 5, '10001', 8, 9),
(56, 5, '10002', 1, 7),
(57, 5, '10002', 3, 9),
(58, 5, '10002', 5, 5),
(59, 5, '10002', 7, 2),
(60, 5, '10002', 9, 9),
(61, 1, '10001', 1, 7),
(62, 1, '10001', 3, 9),
(64, 4, '10001', 1, 9),
(65, 4, '10001', 7, 8),
(66, 4, '10001', 4, 9),
(67, 4, '10001', 4, 4),
(68, 4, '10001', 5, 9);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `modelno` varchar(50) NOT NULL,
  `modelname` varchar(50) NOT NULL,
  `specifications` varchar(500) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`modelno`, `modelname`, `specifications`) VALUES
('10001', 'Dell Chromebook 11 3180', 'Processor Intel Celeron N3060\r\nGraphics adapterIntel HD Graphics 400 (Braswell)\r\nMemory2048 MB  \r\nDisplay11.6 inch 16:9, 1366 x 768 pixel 135 PPI, LED, glossy: no\r\nStorage16 GB eMMC Flash, 16 GB  \r\n'),
('10002', 'Dell Inspirion 5555', '1.8 GHz with Turbo Boost upto 3.2 GHz\r\nAMD A10-8700P Processor\r\n8GB RAM\r\n1TB Hard Drive\r\n15.6-inch Display\r\n2GB AMD Radeon R6 M345DX Graphics\r\nWindows 8.1 Operating System'),
('10003', 'Dell G5 15 5587', '8th Generation Intel Core i5-8300H Processor (Quad-Core, 8MB Cache, up to 3.9GHz w/Turbo Boost)\r\n8GB 2666MHz DDR4 up to [32GB], (additional memory sold separately)\r\n128 GB (SSD) Boot + 1 TB 5400 RPM [SATA] HDD Storage\r\n15.6-inch FHD (1920 x 1080) IPS Anti-Glare, LED-Backlit Display\r\nBe more productive. Windows 10 is the best for bringing ideas forward and getting things done'),
('10004', 'Dell Inspirion 5749', 'CPU. Intel Core i7-5500U 101.\r\nGPU. NVIDIA GeForce 840M (2GB DDR3) 129.\r\nDisplay. 17.3”, Full HD (1920 x 1080), TN.\r\nHDD/SSD. 1TB HDD, 5400 rpm.\r\nRAM. 1x 8GB DDR3.\r\nOS. Windows OS.\r\nBattery. 40Wh, 4-cell.\r\nBody material. Aluminum, Plastic.'),
('10005', 'Dell Alienware m15', 'CPU. Intel Core i7-8750H 8.\r\nGPU. NVIDIA GeForce GTX 1070 Max-Q (8GB GDDR5) 15.\r\nDisplay. 15.6”, 4K UHD (3840 x 2160), IPS.\r\nHDD/SSD. 1TB HDD.\r\nRAM. 32GB DDR4, 2666 MHz.\r\nOS.\r\nBattery. 90Wh.\r\nDimensions. 363 x 275 x 20.99 mm (14.29\" x 10.83\" x 0.83\")');

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `serviceno` varchar(20) NOT NULL,
  `servicetype` varchar(50) NOT NULL,
  `servicename` varchar(100) NOT NULL,
  `servicecost` float NOT NULL,
  `description` varchar(1000) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`serviceno`, `servicetype`, `servicename`, `servicecost`, `description`) VALUES
('1', 'IT Infrastructure Services', 'PROSUPPORT ENTERPRISE SUITE', 6000, 'Key Benefits\r\n\r\nFlexibility to choose support based on criticality of specific systems and the complexity of your environment\r\nA central point of accountability for all your hardware and software issues\r\nCross-domain experience that goes beyond a single piece of hardware\r\nPredictive, automated tools and innovative technology\r\nConsistent experience regardless of where you’re located or what language you speak'),
('2', 'IT Infrastructure Services', 'OPTIMIZE FOR STORAGE', 7500, 'Key Benefits\r\n\r\nMaximizes storage performance and operational efficiencies \r\nReduces risk of data loss, downtime or business delays \r\nSaves you time and resources in managing your storage environment \r\nDelivers accelerated returns on your storage investment'),
('3', 'IT Infrastructure Services', 'PERSONALIZED SUPPORT SERVICES', 5600, 'Key Benefits\r\nIncreased support for particular products\r\nCoordinated service events\r\nStreamlined problem resolution'),
('4', 'Business Client Services', 'PROSUPPORT CLIENT SUITE', 6500, 'Key Benefits \r\n\r\nResolves issues 11x faster than competitors1\r\nAvoids or minimizes end-user downtime\r\nOnsite service 24x7, around the world\r\nRepairs for accidental damage\r\nWarning alerts for issues\r\nCan pay for itself with a single incident'),
('5', 'Business Client Services', 'KEEP YOUR HARD DRIVE', 7000, 'Key Benefits\r\n\r\nGreater security – sensitive data and files never leave your control\r\nControl – you decide how and when to dispose of your data\r\nCompliance – helps you comply with privacy regulations and internal company policies\r\nValue – covers multiple drives and multiple failures in a single system'),
('6', 'Support Services for Home PCs', 'PREMIUM SUPPORT PLUS', 5500, 'Premium Support Plus offers the most features in one plan for your Dell PCs. Support when you need it, and step-by-step help when you want it.\r\n\r\n24x7 access to expert hardware and software support\r\nPersonalized help with software for complex issues\r\nHelp with setting data backup, parental controls and more'),
('7', 'Support Services for Home PCs', 'ACCIDENTAL DAMAGE SERVICE', 5600, 'Recover quickly from unexpected surges, drops or spills with Accidental Damage Service.2\r\n\r\nFast repair or replacement for liquid spilled on or in unit\r\nProtection for everyday challenges like drops, falls and other collisions'),
('8', 'Warranty Services', 'WARRANTY EXTENSION', 3500, 'Get peace of mind knowing your Dell and Dell EMC systems will be safer, for longer. Take this opportunity to extend or upgrade your support service.\r\n\r\nExtend your hardware warranty or service contract\r\nUpgrade to experience ProSupport or ProSupport Plus'),
('9', 'Warranty Services', 'FIX YOUR PC OR TABLET', 3300, 'Let our experts get your Dell PC running smoothly. Whether your computer is broken or slowing down, don’t worry – we’re here to help.\r\n\r\nRepair of nearly any type of hardware issue\r\nHelp with software issues, updates or virus removal');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`customerid`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`modelno`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`serviceno`),
  ADD UNIQUE KEY `servicename` (`servicename`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `customer`
--
ALTER TABLE `customer`
  MODIFY `customerid` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
