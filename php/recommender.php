<?php

    $row = 9; // no of services
    $col = 5; // no of customers

    $result=array(array());
    for ($i = 0; $i < $row; $i++) {
        for ($j = 0; $j < $col; $j++) {
            $result[$i][$j] = 0;
        }
    }

    /*for ($i = 0; $i < $row; $i++) {
        for ($j = 0; $j < $col; $j++) {
            echo $result[$i][$j]." ";
        }
        echo "<br/>";
    }*/
    //4 6 7 2 3 8 1 9 5
    include("DBconnect.php");
    $email = $_COOKIE['email']; // take from cookie
    $rows = mysqli_query($conn,"select customerid from customer where email = '$email'");
    $r = mysqli_fetch_array($rows);
    $userid = $r[0];
    for ($i=0; $i < $row; $i++) {
        for ($j=0; $j < $col; $j++) { 
            $rows = mysqli_query($conn,"select avg(rating) from data where serviceno='$i'+1 and customerid='$j'+1 and modelno='$modelno' and rating is not NULL");
            $r=mysqli_fetch_array($rows);
            if ($r[0] == null)
                $result[$i][$j] = 0;
            else {
                $result[$i][$j] = $r[0];
            }
        }
    }


    for ($i=0; $i < $col; $i++) {
        $total = 0;
        $count = 0;
        for ($j=0; $j < $row; $j++) { 
            if ($result[$j][$i] != 0) {
                $total += $result[$j][$i]; 
                $count++;
            }
        }
        $avg = 0;
        if ($count != 0)
            $avg = $total / $count;
        for ($j = 0; $j < $row; $j++) {
            if ($result[$j][$i] != 0)
             $result[$j][$i] -= $avg; 
        }
    }
    
    // getting max matched attributes
    $maxmatch = 0;
    for ($i = 0; $i < $col; $i++) {
        if ($i == $userid - 1)
            continue;

        $max = 0;
        for ($j = 0; $j < $row; $j++) {
            if ($result[$j][$userid-1] == 0)
                continue;
            if ($result[$j][$i] > 0 && $result[$j][$userid-1] > 0) {
                $max++;
            }
            if ($result[$j][$i] < 0 && $result[$j][$userid-1] < 0) {
                $max++;
            }
        }
        if ($max > $maxmatch) {
            $maxmatch = $max;
        }
    }
    
    // getting array of similar users
    $similarusers = array();
    $n = 0;
    for ($i = 0; $i < $col; $i++) {
        if ($i == $userid - 1)
            continue;
        
        $max = 0;
        for ($j = 0; $j < $row; $j++) {
            if ($result[$j][$userid-1] == 0)
                continue;
            if ($result[$j][$i] > 0 && $result[$j][$userid-1] > 0) {
                $max++;
            }
            if ($result[$j][$i] < 0 && $result[$j][$userid-1] < 0) {
                $max++;
            }
        }
        if ($max == $maxmatch) {
           $similarusers[$n] = $i;
           $n++; 
        }
    }

    // mean of similar users
    $y = 0;
    for ($i = 0; $i < $row; $i++) {
        if ($result[$i][$userid-1] != 0) {
            continue;
        }
        $total = 0;
        $count = 0;
        for ($j = 0; $j < $n; $j++) {
            if ($result[$i][$similarusers[$j]] != 0) {
                $total += $result[$i][$similarusers[$j]];
                $count++;
            }
        }
        $avg = 0;
        if ($count != 0) {
            $avg = $total / $count;
        }

        $result[$i][$userid - 1] = $avg;
    }

    $services = array();
    $collabrating = array();
    for ($i = 0; $i < $row; $i++) {
        $services[$i] = $i + 1;
        $collabrating[$i] = $result[$i][$userid - 1];
    }

    // sorting the services for user
    for ($i = 0; $i < $row - 1; $i++) {
        $max = $collabrating[$i];
        $max_index = $i;
        for ($j = $i + 1; $j < $row; $j++) {
            if ($collabrating[$j] > $max) {
                $max = $collabrating[$j];
                $max_index = $j;
            }
        }

        //swap i and max
        $t1 = $services[$i];
        $services[$i] = $services[$max_index];
        $services[$max_index] = $t1;

        $t2 = $collabrating[$i];
        $collabrating[$i] = $collabrating[$max_index];
        $collabrating[$max_index] = $t2;
    }
    
    $recommended = array();
    $size = 0;
    //service no in descending order of collaborative rating
    for ($i = 0; $i < $row; $i++) {
        if ($collabrating[$i] >= 0) {
            $recommended[$size] = $services[$i];
            $size++;
        }
    }
    //echo "<br/>";

    //echo $maxmatch."<br/>";

    /*for ($i = 0; $i < $row; $i++) {
        echo $collabrating[$i]."  ";
    }
    echo "<br/>";
    echo $maxmatch."<br>";*/

    /*for ($i = 0; $i < $row; $i++) {
        for ($j = 0; $j < $col; $j++) {
            echo $result[$i][$j]." ";
        }
        echo "<br/>";
    }*/


?>