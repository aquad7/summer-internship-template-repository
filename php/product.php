<?php
    if(!isset($_COOKIE['email'])) {
        echo "<script>window.location.href='localhost/hackathon/'</script>";
    } else {
        include('DBconnect.php');
        $email = $_COOKIE['email'];
        $result=mysqli_query($conn,"select customerid from customer where email='$email'");
        $row=mysqli_fetch_array($result);
        $custid;
        if ($row[0] == null)
            echo "<script>window.location.href = 'http://localhost/hackathon/home.html'</script>";
        else
            $custid = $row['customerid'];
    }
?>
<?php
    $user=$_COOKIE['email'];
    $modelno = $_POST['modelno'];
    $result=mysqli_query($conn,"select * from product where modelno='$modelno'");
    $row=mysqli_fetch_array($result);
    if ($row[0] == null)
        echo "<script>window.location.href = 'http://localhost/hackathon/home.html'</script>";
?>
<html>
    <head>
        <title>Product</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.7.2/animate.min.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Lato" rel="stylesheet" type="text/css">
        <link rel="stylesheet" href="../css/home.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <link rel="stylesheet" href="animate.min.css">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
        <style>
            .rating {
            display: inline-block;
            position: relative;
            height: 35px;
            line-height: 35px;
            font-size: 35px;
            }

            .rating label {
            position: absolute;
            top: 0;
            left: 0;
            height: 100%;
            cursor: pointer;
            }

            .rating label:last-child {
            position: static;
            }

            .rating label:nth-child(1) {
            z-index: 10;
            }

            .rating label:nth-child(2) {
            z-index: 9;
            }

            .rating label:nth-child(3) {
            z-index: 8;
            }

            .rating label:nth-child(4) {
            z-index: 7;
            }

            .rating label:nth-child(5) {
            z-index: 6;
            }

            .rating label:nth-child(6) {
            z-index: 5;
            }

            .rating label:nth-child(7) {
            z-index: 4;
            }

            .rating label:nth-child(8) {
            z-index: 3;
            }

            .rating label:nth-child(9) {
            z-index: 2;
            }

            .rating label:nth-child(10) {
            z-index: 1;
            }

            .rating label input {
            position: absolute;
            top: 0;
            left: 0;
            opacity: 0;
            }

            .rating label .icon {
            float: left;
            color: transparent;
            }

            .rating label:last-child .icon {
            color: #000;
            }

            .rating:not(:hover) label input:checked ~ .icon,
            .rating:hover label:hover input ~ .icon {
            color: #09f;
            }

            .rating label input:focus:not(:checked) ~ .icon:last-child {
            color: #000;
            text-shadow: 0 0 5px #09f;
            }
        </style>
    </head>
    <body>
        <div class="navbar navbar-default">
        <div class="container">
            <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
            </button>
            <a class="navbar-brand" href="#">DELL Services</a>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="http://localhost/hackathon/home.html"><span class="glyphicon glyphicon-home"></span> HOME</a></li>
            </ul>
            </div>
        </div>
        </div>
        <div style="background-color:white;box-shadow:5px 2px 8px;" class="container-fluid">
        <div class="row">
            <div class="col-md-3"></div>
            <center class="col-md-6 product-details">
                <img src="http://localhost/hackathon/product-image/<?php
                    echo $row['modelno'];
                ?>.png" height="200" width="300" class="img-thumbnail" /><br/><br/>
                <b>Name</b> : <?php echo $row['modelname']; ?><br/><br/>
                <b>Specifications</b> : <?php echo $row['specifications']; ?>
            </center>
            <div class="col-md-3"></div>
        </div>    
        </div>
        <br/>
        <div class="container-fluid text-center">
        <h2 style="">Recommended Services</h2>
        <br>
        <div class="row">
            <?php
                include('recommender.php');
                for ($i = 0; $i < $size; $i++) {
                    $result = mysqli_query($conn, "select * from services where serviceno ='$recommended[$i]'");
                    $row = mysqli_fetch_array($result);
                    echo "<div class=\"col-sm-4 padder serviceback\">";
                    echo "<div class=\"panel\">";
                    echo "<div class\"panel-heading\">";
                    echo "<img src=\"http://localhost/hackathon/service-image/".$row['serviceno'].".png\" class=\"serviceimage\"></div>";
                    echo "<div style=\"text-align: left\" class=\"panel-body\">";
                    echo "<span style=\"color: dodgerblue\">".$row["servicetype"]."</span>";
                    echo "<h2>".$row['servicename']."</h2>";
                    echo "<a href=\"service.php?serviceid=".$recommended[$i]."&modelno=".$modelno."\"><button class=\"mybutton\">Know More</button></a>";
                    echo "</div></div></div>";
                }
            ?>
        </div>
        </div>
        <br/>
        <div class="container-fluid text-center">
        <h2 style="">FEEDBACK</h2>
        <div class="row">
        <center id="feedbackdiv" class="col-sm-12">
        </center>
        </div>
        </div>
        <script>
            var feedbackdiv = document.getElementById("feedbackdiv");
            var stars = 0;
            var id = null;

            getPendingFeedbacks();
            function getPendingFeedbacks () {
                var hr = new XMLHttpRequest();
                hr.onreadystatechange = function () {
                    if (hr.readyState !== 4)
                    return;

                    if (hr.status === 200) {
                        if (hr.responseText === "") {
                            feedbackdiv.innerHTML = "NO PENDING FEEDBACKS<br/><br/><br/>";
                        } else {    
                            var arr = hr.responseText.split(";");
                            id = arr[0];
                            var content = `
                                ${arr[1]}<br/>
                                <div class="rating">
                                    <label>
                                        <input onclick="countStars(1);" type="radio" name="stars" value="1" />
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(2);" type="radio" name="stars" value="2" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(3);" type="radio" name="stars" value="3" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>   
                                    </label>
                                    <label>
                                        <input onclick="countStars(4);" type="radio" name="stars" value="4" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(5);" type="radio" name="stars" value="5" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(6);" type="radio" name="stars" value="6" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(7);" type="radio" name="stars" value="7" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(8);" type="radio" name="stars" value="8" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(9);" type="radio" name="stars" value="9" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                    <label>
                                        <input onclick="countStars(10);" type="radio" name="stars" value="10" />
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                        <span class="icon">★</span>
                                    </label>
                                </div>
                                <br/>
                                <button onclick="sendFeedBack();" class="mybutton">Submit</button><br/><br/>
                            `;
                            feedbackdiv.innerHTML = content;
                        }
                    } else if (hr.status === 404) {
                        alert("page not found");
                    } else if (hr.status === 500) {
                        alert("Internal server error");
                    } else {
                        alert("some error occured");
                    }
                }

                hr.open('POST', 'http://localhost/hackathon/php/pending.php');
                hr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                hr.send('modelno=' + <?php echo $modelno ?> + "&custid=" + <?php echo $custid ?>);
            }

            function sendFeedBack () {
                if (stars === 0)
                    alert('please provide some rating!');
                else {
                    var hr = new XMLHttpRequest();
                    hr.onreadystatechange = function () {
                        if (hr.readyState !== 4)
                        return;

                        if (hr.status === 200) {
                            if (hr.responseText !== "1") {
                                feedbackdiv.innerHTML = "SOME ERROR OCCURRED TRY AGAIN!<br/><br/><br/>";
                                setTimeout(getPendingFeedbacks, 1000);
                            } else {
                                feedbackdiv.innerHTML = "THANK YOU FOR GIVING US YOUR VALUABLE TIME!<br/><br/><br/>";
                                setTimeout(getPendingFeedbacks, 1000);
                            }
                            stars = 0;
                        } else if (hr.status === 404) {
                            alert("page not found");
                        } else if (hr.status === 500) {
                            alert("Internal server error");
                        } else {
                            alert("some error occured");
                        }
                    }
                    hr.open('POST', 'http://localhost/hackathon/php/feedback.php');
                    hr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    hr.send('stars=' + stars + "&id=" + id);
                }
            }

            function countStars (count) {
                stars = count;
                //alert(stars);
            }
        </script>
    </body>
</html>