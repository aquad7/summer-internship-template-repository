## AQUAD (GROUP 3) - Service Support Recommendation

### Project Overview
----------------------------------

A brief description of 
* Service Support Recommendation

* In this project we tried to implement a Service Recommendation Engine using the Collaborative Filtering approach which uses the similarity of the ratings provided by the users of the same model number to recommend services to other users of the same model. 

### Solution Description
----------------------------------

#### Architecture Diagram

![Architecture](Screenshot from 2019-11-10 23-18-55.png)

#### Technical Description


* ##### Technology Used:
        1. PHP for Backend.
        2. Apache Web Server.
        3. MySQL for creating database.
        4. HTML, CSS, Bootstrap for frontend.

* ##### Setup/Installation Required (Windows)
        1. XAMPP (MySQL DB + Apache Web Server) installation.
            Install the XAMPP Application on windows normally following the setup instructions. 
* ##### Steps to run the solution
        1. After installation run the XAMPP Control panel and start the Apache Web Server and MySQL database server.

        2. From the 'config' options in XAMPP control panel set browser location to Google Chrome executable directory. Save the changes.

        3. Move the hackathon project folder to the C:/XAMPP/htdocs.

        4. In Chrome navigate to the URL localhost/hackathon, and you will be presented with the login page.

### Team Members
----------------------------------
##### 1. Auroshis Ray  ---> User Interface Development & integration with backend.
##### 2. Abhijeet Choudhuri ---> User Interface Development & integration with backend.
##### 3. Abhinash Dash ---> Database Design & Backend Development.
##### 4. Arna Maity ---> Database Design & Backend Development.
